 /*
Config program Bluetooth

http://www.jnhuamao.cn/bluetooth.asp?id=1
HC03 HC05 LF+CR
HC04 HC06 no LF+CR
BLE CC2541 without crystal send with LF + CR
HM-10 (CC2541 with crystal) no LF+CR
JDY-30 JDY-31 JDY-33 LF+CR

https://docs.espressif.com/projects/esp-at/en/latest/esp32c3/AT_Command_Set/Basic_AT_Commands.html

AT : check the connection
AT+NAME: Change name. No space between name and command.
AT+BAUD: change baud rate, x is baud rate code, no space between command and code.
4: 9600, 5: 19200, 6: 38400, 7: 57600, 8: 115200
AT+PIN: change pin, xxxx is the pin, again, no space.
AT+VERSION
AT+HELP list of command
*/

#define RX 12 //12 TX module bluetooth
#define TX 13 //13 RX module bluetooth
#define SPEED 9600 //9600 by default
#include <SoftwareSerial.h>
SoftwareSerial BTserial(RX,TX);
#define pinGnd 11 //version 1.0.3


void setup()
{
  pinMode(pinGnd,OUTPUT);
  digitalWrite(pinGnd,0);
  Serial.begin(SPEED);
  while (!Serial) ; // Needed for native USB port only
  Serial.println("Hello !");
  Serial.println("Enter AT commands:");
  // set the data rate for the SoftwareSerial port
  BTserial.begin(SPEED);
}

void loop() {

  if (BTserial.available())
  {
    byte serialA = BTserial.read();
    Serial.write(serialA);
  }
  if (Serial.available())
    BTserial.write(Serial.read());
}
