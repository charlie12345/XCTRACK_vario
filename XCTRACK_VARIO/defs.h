#ifndef DEFS_h
#define DEFS_h
#include "MS5611.h"

#define VERSION "1.0.5"

#define FREQUENCY 12  // data output in Hz : adjust value for frequency mode

#ifndef __AVR_ATtiny85__
#define USB_MODE false  // no need if you use bluetooth
#define BLUETOOTH_MODE true
#define EXTRA_INFOS_PERIOD FREQUENCY*60 // 60s
#define TEST_MODE false //just for testing without ms5611
#endif

#ifdef ESP32
  #define pinMS5611Gnd 4
  #define pinSCL 3
  #define pinSDA 2
  #define pinBattery A0
  #include "ble_uart.h"
#else
  #if BLUETOOTH_MODE
    #include <SoftwareSerial.h>
    #define BLUETOOTH_SPEED 9600 //bluetooth speed (9600 by default)
    #define pinSPPGnd 11 //version 1.0.3
    #define RX 12 // unnecessary (TX module bluetooth)
    #define TX 13 //version1.0.3:13 version 1.0.1:12 (RX module bluetooth)
    const uint8_t pinAutoCutOff[]={5,6,7};
    #define AUTO_CUT_OFF_PERIOD FREQUENCY*20 // IP5306 32s see datasheet
    #define AUTO_CUT_OFF_DELAY FREQUENCY*1 // 1s
  #endif
  #include <avr/wdt.h>
  #define pinBattery A0
#endif

#if USB_MODE
#define USB_SPEED 9600  //serial transmision speed
#endif

const char MESSAGE_VERSION[] = "XCTRACK VARIO V" VERSION;
const char MESSAGE_COMPILE_DATE[] = "compiled " __DATE__;
const char MESSAGE_CHECK[] = "Checking MS5611 sensor...";
const char MESSAGE_ERROR[] = "MS5611 error";
const char MESSAGE_RESET[] = "Reset in 1s...";
const char MESSAGE_BLUETOOTH[] = "Bluetooth mode";
const char MESSAGE_BLE_ADVERTISING[] = "Waiting a client connection to notify...";

const char FORMAT_XCTOD[]="$XCTOD,%d,%u\n"; //$XCTOD,field1,field2,...,field50[\r]\n

enum sentence_id{
  S_VARIO,
  S_LK8EX1,
	S_PRS
};

const char FORMATS[3][32] ={
  "$VARIO,%s,,",
  "$LK8EX1,%lu,9,9999,99,999,",
   #ifdef ESP32
  "PRS %X\n",
    #else
  "PRS %lX\n"
    #endif
};

class VARIO{
  private:
    void trimChar(char * s);
    uint8_t measureBattery();
    sentence_id sentence;

  public:
    VARIO(sentence_id s = S_VARIO);
    void begin();
    void send_bluetooth();
    void collect_extra_infos();
    void format_sentence();
    void send_error();
    char str_out[128];
    MS5611 sensor;
    #ifdef __AVR_ATtiny85__
      uint32_t pressure;
    #else
      double pressure;
      #if BLUETOOTH_MODE
        #ifdef ESP32
          BLE ble;
        #else
          SoftwareSerial *BTserial;
        #endif
      #endif
    #endif

};

#endif